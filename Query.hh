<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\queryBuilder\driver\mysql
{
	class Query
	{
		private Vector<string> $where	=Vector{};
		private Vector<string> $values	=Vector{};
		
		public function addWhere(string $wherePart):this
		{
			$this->where[]=$wherePart;
			return $this;
		}
		
		public function addValue(mixed $value):this
		{
			$this->values[]=$value;
			return $this;
		}
	}
}
