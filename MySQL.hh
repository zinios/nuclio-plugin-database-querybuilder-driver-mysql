<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\queryBuilder\driver\mysql
{
	use nuclio\core\
	{
		lassManager,
		plugin\Plugin
	};
	use nuclio\plugin\database\
	{
		queryBuilder\QueryBuilder,
		queryBuilder\exception\QueryBuilderException,
		queryBuilder\CommonQueryBuilderInterface,
		queryBuilder\ExecutionParams,
		queryBuilder\Wheres,
		queryBuilder\Values,
		queryBuilder\WheresAndValues,
		datasource\source\Source,
		orm\Model,
		common\DBRecord,
		common\DBVector,
		common\DBQuery,
		common\DBFields,
		common\DBCondition,
		common\CommonCursorInterface,
		exception\DatabaseException
	};
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	
	<<
		factory,
		provides('queryBuilder::mysql','queryBuilder::mysqlPDO')
	>>
	class MySQL extends Plugin implements CommonQueryBuilderInterface
	{
		const string ORDER_ASCENDING				='ASC';
		const string ORDER_DECENDING				='DESC';
		
		const string OPERATOR_EQUALS				='=';
		const string OPERATOR_NOT_EQUALS			='!=';
		const string OPERATOR_REGEX					='REGEXP';
		const string OPERATOR_GREATER_THAN			='>';
		const string OPERATOR_GREATER_THAN_EQUALS	='>=';
		const string OPERATOR_LESS_THAN				='<';
		const string OPERATOR_LESS_THAN_EQUALS		='<=';
		const string OPERATOR_IN					='IN';
		const string OPERATOR_NOT_IN				='NOT IN';
		
		private QueryBuilder $queryBuilder;
		private Source $source;
		
		public static function getInstance(QueryBuilder $queryBuilder, Source $source):MySQL
		{
			$instance=ClassManager::getClassInstance(self::class,$source);
			return ($instance instanceof self)?$instance:new self($source);
		}
		
		public function __construct(QueryBuilder $queryBuilder, Source $source)
		{
			parent::__construct();
			$this->queryBuilder=$queryBuilder;
			$this->source=$source;
		}
		
		public function execute(ExecutionParams $params):CommonCursorInterface
		{
			$params	=new Map((array)$params);
			$target	=$params->get('target');
			$filter	=$params->get('filter');
			$limit	=$params->get('limit');
			$offset	=$params->get('offset');
			$orderBy=$params->get('orderBy');
			$joins	=$params->get('joins');
			
			if (is_null($target))
			{
				throw new QueryBuilderException('Unable to execute query. Missing target parameter.');
			}
			if (is_null($filter))
			{
				throw new QueryBuilderException('Unable to execute query. Missing filter parameter.');
			}
			
			list($where,$values)=$this->parseFilter($target,$filter);
			
			$limitSQL='';
			if(is_numeric($limit) && is_numeric($offset))
			{
				$limitSQL="LIMIT $offset,$limit";
			}
			else if (is_numeric($limit))
			{
				$limitSQL="LIMIT $limit";
			}
			
			$orderBySQL=[];
			if (!is_null($orderBy))
			{
				if ($orderBy instanceof Map)
				{
					foreach ($orderBy as $column=>$direction)
					{
						$orderBySQL[]=sprintf('`%s` %s',$column,$direction);
					}
				}
				else if (is_string($orderBy))
				{
					$orderBySQL[]=$orderBy;
				}
			}
			if (count($orderBySQL))
			{
				$orderBySQL='ORDER BY '.implode(',',$orderBySQL);
			}
			else
			{
				$orderBySQL='';
			}
			
			$query=<<<SQL
			SELECT *
			FROM {$target}
			{$where}
			{$orderBySQL}
			{$limitSQL};
SQL;

			// var_dump($query);
			$this->queryBuilder->setLastQuery($query);
			// $cursor=ProviderManager::request('database::mysql::cursor',$this->source->query($query,$values));
			try
			{
				$result=$this->source->query($query,$values);
				return $result;
			}
			catch (\Exception $exception)
			{
				$exception=new DatabaseException($exception->getMessage());
				$exception->setDebug
				(
					Map
					{
						'query'=>$query
					}
				);
				throw $exception;
			}
			// if ($cursor instanceof CommonCursorInterface)
			// {
			// 	return $cursor;
			// }
			// else
			// {
			// 	throw new QueryBuilderException('Expected an instance of CommonCursorInterface but didn\'t receive one from the Provider Manager. Please check that you are using a compatible driver.');
			// }
		}
		
		public function parseFilter(string $target, DBQuery $filter):WheresAndValues
		{
			$values=Vector{};
			if (count($filter))
			{
				$first=true;
				$where=Vector{};
				foreach ($filter as $condition=>$thisFilter)
				{
					if (!$first)
					{
						$where[]='AND';
					}
					$first=false;
					switch ($condition)
					{
						case '$or':
						{
							if (count($thisFilter) && is_int(array_keys($thisFilter)[0]))
							{
								$where[]='(';
								$theseWheres=Vector{};
								for ($i=0,$j=count($thisFilter); $i<$j; $i++)
								{
									// $theseWheres[]='(';
									$andWheres=Vector{};
									foreach ($thisFilter[$i] as $field=>$value)
									{
										$thisTableName=$target;
										if (is_array($value))
										{
											$operator	=key($value);
											$value		=current($value);
											list($andWheres,$values)=$this->parseOperator($andWheres,$values,$thisTableName,$field,$operator,$value);
										}
										else
										{
											list($andWheres,$values)=$this->buildCondition($andWheres,$values,'=',$thisTableName,$field,$value);
										}
									}
									$theseWheres[]='('.(implode(' AND ',$andWheres)).')';
									// $theseWheres[]=')';
								}
								// $theseWheres=implode('',$theseWheres);
								$where[]=implode(' OR ',$theseWheres);
								$where[]=')';
							}
							else
							{
								$theseWheres=Vector{};
								foreach ($thisFilter as $field=>$value)
								{
									$thisTableName=$target;
									if (is_array($value))
									{
										$operator	=key($value);
										$value		=current($value);
										list($theseWheres,$values)=$this->parseOperator($theseWheres,$values,$thisTableName,$field,$operator,$value);
									}
									else
									{
										list($theseWheres,$values)=$this->buildCondition($theseWheres,$values,'=',$thisTableName,$field,$value);
									}
								}
								$where[]=implode(' OR ',$theseWheres);
								$where[]=')';
							}
							break;
						}
						case '$and':
						{
							$where[]='(';
							$theseWheres=Vector{};
							foreach ($thisFilter as $field=>$value)
							{
								$thisTableName=$target;
								if (is_array($value))
								{
									$operator	=key($value);
									$value		=current($value);
									list($theseWheres,$values)=$this->parseOperator($theseWheres,$values,$thisTableName,$field,$operator,$value);
									
								}
								else
								{
									list($theseWheres,$values)=$this->buildCondition($theseWheres,$values,'=',$thisTableName,$field,$value);
								}
							}
							$where[]=implode(' AND ',$theseWheres);
							$where[]=')';
							break;
						}
						default:
						{
							$field			=$condition;
							$value			=$thisFilter;
							$thisTableName	=$target;
							// if (is_array($value))
							// {
							// 	$operator	=key($value);
							// 	$value		=current($value);
							// 	list($where,$values)=$this->parseOperator($where,$values,$thisTableName,$field,$operator,$value);
							// }
							// else
							if ($value instanceof Pair)
							{
								$operator		=$value->get(0);
								$value			=$value->get(1);
								list($where,$values)=$this->parseOperator($where,$values,$thisTableName,$field,$operator,$value);
							}
							else if (is_array($value))
							{
								for ($i=0,$j=count($value); $i<$j; $i++)
								{
									if ($value[$i] instanceof Pair)
									{
										$operator		=$value[$i]->get(0);
										$thisValue		=$value[$i]->get(1);
										list($where,$values)=$this->parseOperator($where,$values,$thisTableName,$field,$operator,$thisValue);
									}
									else
									{
										list($where,$values)=$this->buildCondition($where,$values,'=',$thisTableName,$field,$value[$i]);
									}
									if ($i+1!=$j)
									{
										$where[]='AND';
									}
								}
							}
							else
							{
								list($where,$values)=$this->buildCondition($where,$values,'=',$thisTableName,$field,$value);
							}
						}
					}
				}
				$where='WHERE '.implode(' ',$where);
			}
			else
			{
				$where='';
			}
			return Pair{$where,$values};
		}
		
		private function buildCondition(Wheres $where,Values $values,string $operator,string $table,string $field, mixed $value):WheresAndValues
		{
			if (!is_null($value))
			{
				switch ($operator)
				{
					case self::OPERATOR_IN:
					case self::OPERATOR_NOT_IN:
					{
						if (!is_array($value))
						{
							$value=[$value];
						}
						$placeholders=[];
						for ($i=0,$j=count($value); $i<$j; $i++)
						{
							$placeholders[]	='?';
							$values[]		=$value[$i];
//							if (!is_null($value[$i]))
//							{
//								$placeholders[]	='?';
//								$values[]		=$value[$i];
//							}
//							else
//							{
//								$placeholders[]	='NULL';
//							}
						}
						$placeholders=implode(',',$placeholders);
						
						$where[]='`'.$table.'`.`'.$field.'` '.$operator.' ('.$placeholders.') ';	break;
						break;
					}
					case self::OPERATOR_EQUALS:
					case self::OPERATOR_NOT_EQUALS:
					case self::OPERATOR_REGEX:
					case self::OPERATOR_GREATER_THAN:
					case self::OPERATOR_GREATER_THAN_EQUALS:
					case self::OPERATOR_LESS_THAN:
					case self::OPERATOR_LESS_THAN_EQUALS:
					{
						// if ($value instanceof Map)
						// {
						// 	foreach ($value as $key=>$val)
						// 	{
						// 		$where[]='`'.$table.'`.`'.$field.'` '.$operator.' ('..') ';
						// 		$values[]=$val;
						// 	}
							
						// }
						// else
						// {
							$where[]='`'.$table.'`.`'.$field.'` '.$operator.' (?) ';
							$values[]=$value;
						// }
						break;
					}
				}
			}
			else
			{
				switch ($operator)
				{
					case self::OPERATOR_EQUALS:					$where[]='`'.$table.'`.`'.$field.'` IS NULL';		break;
					case self::OPERATOR_NOT_EQUALS:				$where[]='`'.$table.'`.`'.$field.'` IS NOT NULL';	break;
					case self::OPERATOR_IN:						$where[]='`'.$table.'`.`'.$field.'` IN(NULL)';		break;
					case self::OPERATOR_NOT_IN:					$where[]='`'.$table.'`.`'.$field.'` NOT IN(NULL)';	break;
					case self::OPERATOR_REGEX:					$where[]='`'.$table.'`.`'.$field.'` REGEXP';		break;
					case self::OPERATOR_GREATER_THAN:			break;
					case self::OPERATOR_GREATER_THAN_EQUALS:	break;
					case self::OPERATOR_LESS_THAN:				break;
					case self::OPERATOR_LESS_THAN_EQUALS:		break;
				}
			}
			return Pair{$where,$values};
		}
		
		private function parseOperator(Wheres $where,Values $values,string $tableName,string $field,string $operator,mixed $value):WheresAndValues
		{
			switch ($operator)
			{
				case '$regex':
				{
					list($where,$values)=$this->buildCondition($where,$values,self::OPERATOR_REGEX,$tableName,$field,$value);
					break;
				}
				case '$gt':
				{
					list($where,$values)=$this->buildCondition($where,$values,self::OPERATOR_GREATER_THAN,$tableName,$field,$value);
					break;
				}
				case '$gte':
				{
					list($where,$values)=$this->buildCondition($where,$values,self::OPERATOR_GREATER_THAN_EQUALS,$tableName,$field,$value);
					break;
				}
				case '$lt':
				{
					list($where,$values)=$this->buildCondition($where,$values,self::OPERATOR_LESS_THAN,$tableName,$field,$value);
					break;
				}
				case '$lte':
				{
					list($where,$values)=$this->buildCondition($where,$values,self::OPERATOR_LESS_THAN_EQUALS,$tableName,$field,$value);
					break;
				}
				case '$ne':
				{
					list($where,$values)=$this->buildCondition($where,$values,self::OPERATOR_NOT_EQUALS,$tableName,$field,$value);
					break;
				}
				case '$in':
				{
					list($where,$values)=$this->buildCondition($where,$values,self::OPERATOR_IN,$tableName,$field,$value);
					break;
				}
				case '$nin':
				{
					list($where,$values)=$this->buildCondition($where,$values,self::OPERATOR_NOT_IN,$tableName,$field,$value);
					break;
				}
				default:
				{
					$field=$operator;
					if (is_array($value))
					{
						$operator	=key($value);
						$value		=current($value);
						list($where,$values)=$this->parseOperator($where,$values,$tableName,$field,$operator,$value);
					}
					else
					{
						list($where,$values)=$this->buildCondition($where,$values,'=',$tableName,$field,$value);
					}
				}
			}
			return Pair{$where,$values};
		}
	}
}
